package com.screenmirroring.screencast.models;

import com.google.gson.annotations.SerializedName;

public class AdsModel {

    public boolean status;
    public String message;
    public Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Interstitial{
        public String id;
        public String show_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }
    }

    public static class Native{
        public String id;
        public String show_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }
    }

    public static class Banner{
        public String id;
        public String show_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }
    }

    public static class Open{
        public String id;
        public String show_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }
    }

    public static class Admob{
        public Interstitial interstitial;
        @SerializedName("native")
        public Native mynative;
        public Banner banner;
        @SerializedName("open")
        public Open myopen;

        public Interstitial getInterstitial() {
            return interstitial;
        }

        public void setInterstitial(Interstitial interstitial) {
            this.interstitial = interstitial;
        }

        public Native getMynative() {
            return mynative;
        }

        public void setMynative(Native mynative) {
            this.mynative = mynative;
        }

        public Banner getBanner() {
            return banner;
        }

        public void setBanner(Banner banner) {
            this.banner = banner;
        }

        public Open getMyopen() {
            return myopen;
        }

        public void setMyopen(Open myopen) {
            this.myopen = myopen;
        }
    }

    public static class Publishers{
        public Admob admob;

        public Admob getAdmob() {
            return admob;
        }

        public void setAdmob(Admob admob) {
            this.admob = admob;
        }
    }

    public static class Data{
        public int publisher_id;
        public String publisher_name;
        public Publishers publishers;

        public int getPublisher_id() {
            return publisher_id;
        }

        public void setPublisher_id(int publisher_id) {
            this.publisher_id = publisher_id;
        }

        public String getPublisher_name() {
            return publisher_name;
        }

        public void setPublisher_name(String publisher_name) {
            this.publisher_name = publisher_name;
        }

        public Publishers getPublishers() {
            return publishers;
        }

        public void setPublishers(Publishers publishers) {
            this.publishers = publishers;
        }
    }

}