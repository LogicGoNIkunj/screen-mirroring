package com.screenmirroring.screencast.models;

import java.util.ArrayList;

public class VideoFolderModel {
    String FolderName;
    ArrayList<String> VideoPath;
    String path;
    boolean isExpandable;

    public VideoFolderModel() {
        isExpandable = true;
    }

    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean expandable) {
        isExpandable = expandable;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public ArrayList<String> getVideoPath() {
        return VideoPath;
    }

    public void setVideoPath(ArrayList<String> videoPath) {
        VideoPath = videoPath;
    }
}
