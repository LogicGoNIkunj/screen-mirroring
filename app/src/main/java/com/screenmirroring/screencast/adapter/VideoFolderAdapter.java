package com.screenmirroring.screencast.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.activity.VideoFolderActivity;
import com.screenmirroring.screencast.activity.VideosActivity;
import com.screenmirroring.screencast.models.VideoFolderModel;

import java.util.ArrayList;

public class VideoFolderAdapter extends RecyclerView.Adapter<VideoFolderAdapter.ViewHolder> {

    final Context context;
    final ArrayList<VideoFolderModel> arrayList;
    final Activity activity;

    public VideoFolderAdapter(Context context, VideoFolderActivity activity, ArrayList<VideoFolderModel> videoFolderModelArrayList) {
        this.context = context;
        this.arrayList = videoFolderModelArrayList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.video_folder_list, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "NonConstantResourceId"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.textView.setText(arrayList.get(position).getFolderName());
        holder.textView1.setText(arrayList.get(position).getVideoPath().size() + " Videos");

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, VideosActivity.class);
            intent.putExtra("pos", arrayList.get(position).getFolderName());
            context.startActivity(intent);
            try {
                if (VideoFolderActivity.minterstitialAd != null) {
                    VideoFolderActivity.minterstitialAd.show(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView textView;
        final TextView textView1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.name);
            textView1 = itemView.findViewById(R.id.videos);
        }
    }

}