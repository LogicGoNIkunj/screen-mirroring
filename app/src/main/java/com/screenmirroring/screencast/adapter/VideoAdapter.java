package com.screenmirroring.screencast.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.activity.VideoPlayActivity;
import com.screenmirroring.screencast.activity.VideosActivity;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.models.VideoModel;

import java.io.File;
import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.viewHolder> {

    final Context context;
    final ArrayList<VideoModel> videoArrayList;
    private final ContentResolver cr;
    final Activity activity;



    public VideoAdapter(Context context, VideosActivity activity, ArrayList<VideoModel> videoArrayList) {
        this.context = context;
        this.videoArrayList = videoArrayList;
        this.activity = activity;
        cr = context.getContentResolver();
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.video_list, viewGroup, false);
        return new viewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "NonConstantResourceId", "NotifyDataSetChanged"})
    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        try {
            VideoModel model = videoArrayList.get(position);
            holder.title.setText(model.getVideoTitle());
            holder.duration.setText(model.getVideoDuration());
            holder.size.setText(model.getSize());
            Glide.with(context).load(getbitmap(new File(String.valueOf(videoArrayList.get(position).getVideoUri())))).into(holder.imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(view -> {
            try {
                Intent intent = new Intent(context, VideoPlayActivity.class);
                intent.putExtra("path", videoArrayList.get(position).getVideoUri());
                intent.setData(videoArrayList.get(position).getVideoUri());
                context.startActivity(intent);
                if (VideosActivity.minterstitialAd != null) {
                    VideosActivity.minterstitialAd.show(activity);
                    MyApplication.appOpenAdManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoArrayList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {

        final TextView title, duration, size;
        final ImageView imageView;

        public viewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            duration = itemView.findViewById(R.id.duration);
            imageView = itemView.findViewById(R.id.image);
            size = itemView.findViewById(R.id.size);
        }
    }

    Bitmap getbitmap(File file) {
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.DATA};
        Cursor cursor = cr.query(MediaStore.Video.Media.getContentUri("external"), projection, MediaStore.Images.Media.DATA + "=? ",
                new String[]{file.getPath()}, null);

        if (cursor != null && cursor.moveToNext()) {
            int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            int id = cursor.getInt(idColumn);
            Bitmap thumbNail = MediaStore.Video.Thumbnails.getThumbnail(cr, id, MediaStore.Video.Thumbnails.MINI_KIND, null);
            cursor.close();
            return thumbNail;
        }
        return null;
    }

}