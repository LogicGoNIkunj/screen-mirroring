package com.screenmirroring.screencast.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.FirebaseApp;
import com.screenmirroring.screencast.R;
import com.google.android.gms.ads.MobileAds;

public class MyApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    public static AppOpenManager appOpenAdManager;

    public static SharedPreferences preferences;
    public static SharedPreferences.Editor mEditor;
    @SuppressLint("StaticFieldLeak")
    public static Context context;
    Activity activity;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        MobileAds.initialize(this, initializationStatus -> {
        });
        context = this;
        appOpenAdManager = new AppOpenManager(this);
        preferences = getSharedPreferences("ps", MODE_PRIVATE);
        mEditor = preferences.edit();


    }

    public static void set_AdsInt(int adsInt) {
        mEditor.putInt("adsInt", adsInt).commit();
    }

    public static int get_AdsInt() {
        return preferences.getInt("adsInt", 3);
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        mEditor.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id() {
        return preferences.getString("Admob_interstitial_Id", context.getString(R.string.admob_inter_id));
    }

    //admob banner
    public static void set_Admob_banner_Id(String Admob_banner_Id) {
        mEditor.putString("Admob_banner_Id", Admob_banner_Id).commit();
    }

    public static String get_Admob_banner_Id() {
        return preferences.getString("Admob_banner_Id", context.getString(R.string.admob_banner_id));
    }

    //admob native
    public static void set_Admob_native_Id(String Admob_native_Id) {
        mEditor.putString("Admob_native_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id() {
        return preferences.getString("Admob_native_Id", context.getString(R.string.admob_native_id));
    }

    //admob openapp
    public static void set_Admob_openapp(String Admob_native_Id) {
        mEditor.putString("Admob_open_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_openapp() {
        return preferences.getString("Admob_open_Id", context.getString(R.string.openapp));
    }

}