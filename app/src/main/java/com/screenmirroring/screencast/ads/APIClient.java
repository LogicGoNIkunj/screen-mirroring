package com.screenmirroring.screencast.ads;


import com.screenmirroring.screencast.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static final String BASE_URL_qureka = "https://smartadz.in/api/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Builder().baseUrl(BASE_URL_qureka).client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().addHeader("Authorization", BuildConfig.APPLICATION_ID).addHeader("content-type", "application/json").build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
