package com.screenmirroring.screencast.ads;

import com.screenmirroring.screencast.models.AdsModel;
import com.screenmirroring.screencast.models.Model_Btn;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AdsAPI {
    @FormUrlEncoded
    @POST("get-ads-list")
    Call<AdsModel> getAnswers(@Field("app_id") int id);

    @GET("qureka-ad")
    Call<Model_Btn> getBtnAd();
}