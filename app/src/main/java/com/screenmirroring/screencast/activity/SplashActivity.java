package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.android.gms.ads.MobileAds;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.AdsAPI;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.models.AdsModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    private static final String BASE_URL = "https://stage-ads.punchapp.in/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MobileAds.initialize(this, initializationStatus -> {
        });
        getData();
        runOnUiThread(this::setUpAnimation);
        new Handler().postDelayed(() -> MyApplication.appOpenAdManager.showAdIfAvailable(true), 5000);

    }

    private void getData() {

        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        AdsAPI api = retrofit.build().create(AdsAPI.class);
        Call<AdsModel> call = api.getAnswers(40);
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                MyApplication.set_AdsInt(response.body().getData().getPublisher_id());
                                if (response.body().getData().getPublishers() != null) {
                                    if (MyApplication.get_AdsInt() == 1) {
                                        if (response.body().getData().getPublishers().getAdmob() != null) {
                                            if (response.body().getData().getPublishers().getAdmob().getInterstitial() != null) {
                                                MyApplication.set_Admob_interstitial_Id(response.body().getData().getPublishers().getAdmob().getInterstitial().getId());
                                            } else {
                                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getMynative() != null) {
                                                MyApplication.set_Admob_native_Id(response.body().getData().getPublishers().getAdmob().getMynative().getId());
                                            } else {
                                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getBanner() != null) {
                                                MyApplication.set_Admob_banner_Id(response.body().getData().getPublishers().getAdmob().getBanner().getId());
                                            } else {
                                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getMyopen() != null) {
                                                MyApplication.set_Admob_openapp(response.body().getData().getPublishers().getAdmob().getMyopen().getId());
                                            } else {
                                                MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                                            }

                                        } else {
                                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                                            MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                                        }
                                    } else {
                                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                                        MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                                    }
                                } else {
                                    MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                                    MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                                    MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                                    MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                                }
                            } else {
                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                                MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                            }
                        } else {
                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                            MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                        }
                    } else {
                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                        MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                    }
                } else {
                    MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                    MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                    MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                    MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable throwable) {
                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.admob_banner_id));
                MyApplication.set_Admob_native_Id(getResources().getString(R.string.admob_native_id));
                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.admob_inter_id));
                MyApplication.set_Admob_openapp(getResources().getString(R.string.openapp));
            }
        });

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setUpAnimation() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        ProgressBar progressBar = new ProgressBar(this, null, 16842874);
        Sprite doubleBounce = new ThreeBounce();
        doubleBounce.setColor(getResources().getColor(R.color.white));
        progressBar.setIndeterminateDrawable(doubleBounce);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(110, 110);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, 0, 50);
        relativeLayout.addView(progressBar, layoutParams);
        setContentView(relativeLayout);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}