package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import com.bumptech.glide.Glide;
import com.screenmirroring.screencast.BuildConfig;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.APIClient;
import com.screenmirroring.screencast.ads.AdsAPI;
import com.screenmirroring.screencast.ads.MyApplication;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.screenmirroring.screencast.models.Model_Btn;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    final String appPackageName = BuildConfig.APPLICATION_ID;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;

    //native

    public NativeAd nativeAds;
    FrameLayout frameLayout;

    //interstitial
    InterstitialAd interstitialAd;
    boolean isactivityleft;

    ImageView btnopenad;
    TextView id_text;
    public String qurekaimage = "", querekatext = "", url = "";

    public boolean checkqureka = false;

    @SuppressLint({"NonConstantResourceId,InflateParams"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, initializationStatus -> {
        });
        isactivityleft = false;
        loadBanner();
        frameLayout = findViewById(R.id.fl_adplaceholder);
        loadNative();
        loadInterstitialAd();
        btnopenad = findViewById(R.id.btnopenad);
        id_text = findViewById(R.id.id_text);
        GetUrl();
        findViewById(R.id.back).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.shareApp).setOnClickListener(view -> shareApp());
        findViewById(R.id.rateApp).setOnClickListener(view -> rateApp());
        findViewById(R.id.getStarted).setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, GetStartedActivity.class);
            startActivity(intent);
            try {
                if (interstitialAd != null && !isactivityleft) {
                    interstitialAd.show(MainActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isactivityleft = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isactivityleft = true;
    }

    private void rateApp() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException exception) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void shareApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "HD Video Screen Mirroring");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void loadNative() {
        AdLoader.Builder builder = new AdLoader.Builder(MainActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.unified_nativead, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka(frameLayout, findViewById(R.id.ads_text));
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {

        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                MainActivity.this.interstitialAd = interstitialAd;
                interstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                MainActivity.this.interstitialAd = null;
                                loadInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                MainActivity.this.interstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                interstitialAd = null;
            }
        });
    }

    @Override
    protected void onDestroy() {
        isactivityleft = true;
        if (nativeAds != null) {
            nativeAds.destroy();
        }
        super.onDestroy();
    }

    private void GetUrl() {
        AdsAPI apiinterface = APIClient.getClient().create(AdsAPI.class);

        apiinterface.getBtnAd().enqueue(new Callback<Model_Btn>() {
            @Override
            public void onResponse(@NotNull Call<Model_Btn> call, @NotNull Response<Model_Btn> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                checkqureka = response.body().getData().isFlage();
                                if (response.body().getData().isFlage()) {
                                    btnopenad.setVisibility(View.VISIBLE);
                                    id_text.setVisibility(View.VISIBLE);
                                    querekatext = response.body().getData().getTitle();
                                    qurekaimage = response.body().getData().getImage();
                                    try {
                                        Glide.with(MainActivity.this).load(response.body().getData().getImage()).into(btnopenad);
                                        url = response.body().getData().getUrl();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    btnopenad.setOnClickListener(v -> {
                                        try {
                                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                            CustomTabsIntent customTabsIntent = builder.build();
                                            customTabsIntent.intent.setPackage("com.android.chrome");
                                            customTabsIntent.launchUrl(MainActivity.this, Uri.parse(url));
                                        } catch (Exception e) {
                                            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    btnopenad.setVisibility(View.GONE);
                                    id_text.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Model_Btn> call, @NotNull Throwable t) {
            }
        });
    }

    public void qureka(FrameLayout linearLayout, TextView textView) {
        View view = LayoutInflater.from(this).inflate(R.layout.qureka_native_ads, null);
        TextView ad_call_to_action = view.findViewById(R.id.ad_call_to_action);
        textView.setVisibility(View.GONE);
        linearLayout.removeAllViews();
        linearLayout.addView(view);
        try {
            if (!qurekaimage.equals("")) {
                Glide.with(this).load(qurekaimage).into((ImageView) view.findViewById(R.id.ad_app_icon));
                ((TextView) view.findViewById(R.id.ad_headline)).setText(querekatext);
                ad_call_to_action.setText(querekatext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ad_call_to_action.setOnClickListener(view1 -> {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.intent.setPackage("com.android.chrome");
                customTabsIntent.launchUrl(MainActivity.this, Uri.parse(url));
            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (interstitialAd != null && !isactivityleft) {
                interstitialAd.show(MainActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.finish();
    }

}