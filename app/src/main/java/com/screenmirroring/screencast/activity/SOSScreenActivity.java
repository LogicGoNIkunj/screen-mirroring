package com.screenmirroring.screencast.activity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.MyApplication;

import java.util.Calendar;

@SuppressLint("HandlerLeak")
@SuppressWarnings("ResourceType")
public class SOSScreenActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;
    ObjectAnimator animator;
    private long addedTime;
    private Handler handler;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;

    //interstitial
    InterstitialAd minterstitialAd;
    boolean isactivityleft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosscreen);


        isactivityleft = false;
        loadInterstitialAd();
        loadBanner();

        relativeLayout = findViewById(R.id.relative);

        findViewById(R.id.close).setOnClickListener(view -> {
            onBackPressed();
        });

        start();

    }

    private void start() {
        addedTime = 1;
        final long[] changeTime = {(Calendar.getInstance().getTimeInMillis() / 1000) + addedTime};
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (addedTime == 1) {
                    animator = ObjectAnimator.ofInt(relativeLayout, "backgroundColor", Color.WHITE, Color.BLACK);
                    animator.setDuration(1000);
                    animator.setEvaluator(new ArgbEvaluator());
                    animator.setRepeatCount(Animation.INFINITE);
                    animator.start();
                    addedTime = 5;
                } else {
                    animator = ObjectAnimator.ofInt(relativeLayout, "backgroundColor", Color.WHITE, Color.BLACK);
                    animator.setDuration(200);
                    animator.setEvaluator(new ArgbEvaluator());
                    animator.setRepeatCount(Animation.INFINITE);
                    animator.start();
                    addedTime = 1;
                }
                changeTime[0] = changeTime[0] + addedTime;
                handler.postDelayed(getRunnable(changeTime[0]), 1000);

                super.handleMessage(msg);
            }
        };
        handler.postDelayed(getRunnable(changeTime[0]), 1000);
    }

    private Runnable getRunnable(final long time) {
        return () -> {
            Message message;
            Calendar calendar = Calendar.getInstance();
            if (calendar.getTimeInMillis() / 1000 == time) {
                message = new Message();
                handler.sendMessage(message);
            } else {
                handler.postDelayed(getRunnable(time), 1000);
            }
        };
    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);


        AdRequest adRequest = new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                                loadInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
            }
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        if (animator != null) {
            animator.cancel();
        }
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(SOSScreenActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isactivityleft = true;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }

}