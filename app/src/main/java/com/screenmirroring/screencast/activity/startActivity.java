package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.APIClient;
import com.screenmirroring.screencast.ads.AdsAPI;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.models.Model_Btn;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class startActivity extends AppCompatActivity {

    //native
    public NativeAd nativeAds;
    FrameLayout frameLayout;
    FrameLayout small_frameLayout;

    //interstitial
    InterstitialAd minterstitialAd;
    boolean isactivityleft;
    ImageView btnopenad;
    TextView id_text;
    public String qurekaimage = "", querekatext = "", url = "";
    public boolean checkqureka = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        isactivityleft = false;
        loadInterstitialAd();
        frameLayout = findViewById(R.id.fl_adplaceholder);
        small_frameLayout = findViewById(R.id.fl_adplaceholder2);
        loadNative();
        smallloadNative();
        btnopenad = findViewById(R.id.btnopenad);
        id_text = findViewById(R.id.id_text);
        GetUrl();


        findViewById(R.id.startButton).setOnClickListener(view -> {
            Intent intent = new Intent(startActivity.this, MainActivity.class);
            startActivity(intent);
            try {
                if (minterstitialAd != null && !isactivityleft) {
                    minterstitialAd.show(startActivity.this);
                    MyApplication.appOpenAdManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }

    @SuppressLint("MissingPermission")
    private void loadNative() {
        AdLoader.Builder builder = new AdLoader.Builder(startActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.unified_nativead, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka(frameLayout, findViewById(R.id.ads_text));
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    @SuppressLint("MissingPermission")
    private void smallloadNative() {
        AdLoader.Builder builder = new AdLoader.Builder(startActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.small_native_ad, null, false);
            populateUnifiedNativeAdView2(unifiedNativeAd, adView);
            small_frameLayout.removeAllViews();
            small_frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka_small(small_frameLayout, findViewById(R.id.ads_text2));
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {

        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    private void populateUnifiedNativeAdView2(NativeAd nativeAd, NativeAdView adView) {

        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        adView.setNativeAd(nativeAd);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                                loadInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        isactivityleft = true;
        if (nativeAds != null) {
            nativeAds.destroy();
        }

        super.onDestroy();
    }

    private void GetUrl() {
        AdsAPI apiinterface = APIClient.getClient().create(AdsAPI.class);

        apiinterface.getBtnAd().enqueue(new Callback<Model_Btn>() {
            @Override
            public void onResponse(@NotNull Call<Model_Btn> call, @NotNull Response<Model_Btn> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                checkqureka = response.body().getData().isFlage();
                                if (response.body().getData().isFlage()) {
                                    btnopenad.setVisibility(View.VISIBLE);
                                    id_text.setVisibility(View.VISIBLE);
                                    querekatext = response.body().getData().getTitle();
                                    qurekaimage = response.body().getData().getImage();
                                    try {
                                        Glide.with(startActivity.this).load(response.body().getData().getImage()).into(btnopenad);
                                        url = response.body().getData().getUrl();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    btnopenad.setOnClickListener(v -> {
                                        try {
                                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                            CustomTabsIntent customTabsIntent = builder.build();
                                            customTabsIntent.intent.setPackage("com.android.chrome");
                                            customTabsIntent.launchUrl(startActivity.this, Uri.parse(url));
                                        } catch (Exception e) {
                                            Toast.makeText(startActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    btnopenad.setVisibility(View.GONE);
                                    id_text.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Model_Btn> call, @NotNull Throwable t) {
            }
        });
    }

    public void qureka(FrameLayout linearLayout,TextView textView) {
        View view = LayoutInflater.from(this).inflate(R.layout.qureka_native_ads, null);
        TextView ad_call_to_action = view.findViewById(R.id.ad_call_to_action);
        textView.setVisibility(View.GONE);
        linearLayout.removeAllViews();
        linearLayout.addView(view);
        try {
            if (!qurekaimage.equals("")) {
                Glide.with(this).load(qurekaimage).into((ImageView) view.findViewById(R.id.ad_app_icon));
                ((TextView) view.findViewById(R.id.ad_headline)).setText(querekatext);
                ad_call_to_action.setText(querekatext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ad_call_to_action.setOnClickListener(view1 -> {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.intent.setPackage("com.android.chrome");
                customTabsIntent.launchUrl(startActivity.this, Uri.parse(url));
            } catch (Exception e) {
                Toast.makeText(startActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void qureka_small(FrameLayout linearLayout, TextView textView) {
        View view = LayoutInflater.from(this).inflate(R.layout.small_querka_native, null);
        TextView ad_call_to_action = view.findViewById(R.id.ad_call_to_action);
        textView.setVisibility(View.GONE);
        linearLayout.removeAllViews();
        linearLayout.addView(view);
        try {
            if (!qurekaimage.equals("")) {
                Glide.with(this).load(qurekaimage).into((ImageView) view.findViewById(R.id.ad_querka_icon));
                ((TextView) view.findViewById(R.id.ad_headline)).setText(querekatext);
                ad_call_to_action.setText(querekatext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ad_call_to_action.setOnClickListener(view1 -> {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.intent.setPackage("com.android.chrome");
                customTabsIntent.launchUrl(startActivity.this, Uri.parse(url));
            } catch (Exception e) {
                Toast.makeText(startActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(startActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }

}