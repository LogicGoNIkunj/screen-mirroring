package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.MyApplication;

import java.io.File;

public class VideoPlayActivity extends AppCompatActivity {

    VideoView videoView;
    ImageView pause, zoom;
    SeekBar seekBar;
    int video_index = 0;
    double current_pos, total_duration;
    TextView current, total;
    RelativeLayout showProgress;
    Handler mHandler, handler;
    boolean isVisible = true;
    RelativeLayout relativeLayout, showProgress2;
    Window window;
    ContentResolver cResolver;
    AudioManager audioManager;
    Uri uri;
    int width = 0;
    int height = 0;
    int dimensionsWidth;
    int dimensionsHeight;
    File file;

    //interstitial
    InterstitialAd minterstitialAd;
    boolean isactivityleft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        isactivityleft = false;
        loadInterstitialAd();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        try {
            uri = getIntent().getData();
            file = new File(uri.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        WindowManager windowManager = this.getWindowManager();
        Point size = new Point();
        windowManager.getDefaultDisplay().getSize(size);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        width = size.x;
        height = size.y;

        width = display.getWidth();
        height = display.getHeight();

        dimensionsWidth = width / 2;
        dimensionsHeight = height / 2;

        cResolver = getContentResolver();
        window = getWindow();

        findViewById(R.id.shareButton).setOnClickListener(view -> {
            try {
                Uri createCacheFile = createCacheFile();
                if (createCacheFile != null) {
                    Intent more = new Intent();
                    more.setAction("android.intent.action.SEND");
                    more.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    more.setDataAndType(createCacheFile, getContentResolver().getType(createCacheFile));
                    more.putExtra("android.intent.extra.STREAM", createCacheFile);
                    more.putExtra("android.intent.extra.TEXT", "Personalise more creative photos with this app \nhttps://play.google.com/store/apps/details?id=" + VideoPlayActivity.this.getPackageName());
                    startActivity(Intent.createChooser(more, "Choose an app"));
                    return;
                }
                Toast.makeText(VideoPlayActivity.this, "Fail to sharing", Toast.LENGTH_SHORT).show();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });

        try {
            setVideo();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint({"ClickableViewAccessibility", "SourceLockedOrientationActivity"})
    public void setVideo() {
        videoView = findViewById(R.id.videoview);
        pause = findViewById(R.id.pause);
        seekBar = findViewById(R.id.seekbar);
        current = findViewById(R.id.current);
        total = findViewById(R.id.total);
        showProgress = findViewById(R.id.showProgress);
        showProgress2 = findViewById(R.id.showProgress2);
        relativeLayout = findViewById(R.id.relative);
        zoom = findViewById(R.id.zoom);

        video_index = getIntent().getIntExtra("path", 0);
        mHandler = new Handler();
        handler = new Handler();

        videoView.setOnCompletionListener(mp -> {
            video_index++;
            if (video_index >= (VideosActivity.videoArrayList.size())) {
                video_index = 0;
            }
            playVideo(video_index);
        });

        videoView.setOnPreparedListener(mp -> setVideoProgress());

        int orientation = VideoPlayActivity.this.getResources().getConfiguration().orientation;
        zoom.setOnClickListener(v -> {
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                zoom.setImageResource(R.drawable.fullscreen);
            } else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                zoom.setImageResource(R.drawable.fullscreen_exit);
            }
        });

        playVideo(video_index);
        setPause();
        hideLayout();
    }

    public void playVideo(int pos) {
        try {
            if (uri != null) {
                videoView.setVideoURI(uri);
                videoView.start();
                pause.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
                video_index = pos;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setVideoProgress() {
        current_pos = videoView.getCurrentPosition();
        total_duration = videoView.getDuration();

        total.setText(timeConversion((int) total_duration));
        current.setText(timeConversion((int) current_pos));
        seekBar.setMax((int) total_duration);
        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    current_pos = videoView.getCurrentPosition();
                    current.setText(timeConversion((int) current_pos));
                    seekBar.setProgress((int) current_pos);
                    handler.postDelayed(this, 300);
                } catch (IllegalStateException ed) {
                    ed.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 300);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                current_pos = seekBar.getProgress();
                videoView.seekTo((int) current_pos);
            }
        });
    }

    public void setPause() {
        pause.setOnClickListener(v -> {
            if (videoView.isPlaying()) {
                videoView.pause();
                pause.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
            } else {
                videoView.start();
                pause.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
            }
        });
    }

    @SuppressLint("DefaultLocale")
    public String timeConversion(long value) {
        String videoTime;
        int dur = (int) value;
        int hrs = (dur / 3600000);
        int mns = (dur / 60000) % 60000;
        int scs = dur % 60000 / 1000;

        if (hrs > 0) {
            videoTime = String.format("%02d:%02d:%02d", hrs, mns, scs);
        } else {
            videoTime = String.format("%02d:%02d", mns, scs);
        }
        return videoTime;
    }

    public void hideLayout() {
        final Runnable runnable = () -> {
            showProgress.setVisibility(View.GONE);
            showProgress2.setVisibility(View.GONE);
            isVisible = false;
        };
        handler.postDelayed(runnable, 5000);

        relativeLayout.setOnClickListener(v -> {
            mHandler.removeCallbacks(runnable);
            if (isVisible) {
                showProgress.setVisibility(View.GONE);
                showProgress2.setVisibility(View.GONE);
                isVisible = false;
            } else {
                showProgress.setVisibility(View.VISIBLE);
                showProgress2.setVisibility(View.VISIBLE);
                mHandler.postDelayed(runnable, 5000);
                isVisible = true;
            }
        });
    }

    private Uri createCacheFile() {
        return FileProvider.getUriForFile(getApplicationContext(), getApplicationInfo().packageName + ".provider", file);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
            }
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(VideoPlayActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isactivityleft = true;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }

}