package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.adapter.VideoFolderAdapter;
import com.screenmirroring.screencast.ads.APIClient;
import com.screenmirroring.screencast.ads.AdsAPI;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.models.Model_Btn;
import com.screenmirroring.screencast.models.VideoFolderModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFolderActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<VideoFolderModel> al_images;
    ArrayList<String> al_path;
    VideoFolderModel obj_model;
    private boolean boolean_folder;
    int position = 0;
    Cursor cursor;
    String[] projection;
    String absolutePathOfImage;
    int column_index_data, column_index_folder_name;
    VideoFolderAdapter adapter;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;


    //interstitial
    public static InterstitialAd minterstitialAd;
    boolean isactivityleft;

    ImageView btnopenad;
    TextView id_text;
    public String url = "";

    public boolean checkqureka = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_folder);

        isactivityleft = false;
        loadInterstitialAd();
        loadBanner();
        GetUrl();
        btnopenad = findViewById(R.id.btnopenad);
        id_text = findViewById(R.id.id_text);


        findViewById(R.id.back).setOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.videofolderrecycler);
        al_images = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        try {
            getVideoFolders();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint({"Recycle", "Range"})
    public void getVideoFolders() {
        projection = new String[]{MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME};
        cursor = getApplicationContext().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Video.Media.DATE_TAKEN + " DESC");

        try {
            if (cursor != null) {
                cursor.moveToFirst();
                column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
                do {
                    absolutePathOfImage = cursor.getString(column_index_data);
                    for (int i = 0; i < al_images.size(); i++) {
                        if (al_images.get(i).getFolderName().equals(cursor.getString(column_index_folder_name))) {
                            boolean_folder = true;
                            position = i;
                            break;
                        } else {
                            boolean_folder = false;
                        }
                    }

                    al_path = new ArrayList<>();
                    if (boolean_folder) {
                        al_path.addAll(al_images.get(position).getVideoPath());
                        al_path.add(absolutePathOfImage);
                        al_images.get(position).setVideoPath(al_path);
                    } else {
                        al_path.add(absolutePathOfImage);
                        obj_model = new VideoFolderModel();
                        obj_model.setFolderName(cursor.getString(column_index_folder_name));
                        obj_model.setVideoPath(al_path);
                        obj_model.setPath(getFolderPath(cursor.getString(column_index_data)));
                        al_images.add(obj_model);
                    }

                } while (cursor.moveToNext());


                cursor.close();
            }

            if (al_images.size() > 0) {
                adapter = new VideoFolderAdapter(VideoFolderActivity.this, VideoFolderActivity.this, al_images);
                recyclerView.setAdapter(adapter);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }


    }

    public String getFolderPath(String filePath) {
        String path = filePath;
        Uri uri = Uri.parse(path);
        path = path.replace("/" + uri.getLastPathSegment(), "");
        return path;
    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);

        AdRequest adRequest = new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
            }
        });
    }

    @Override
    protected void onDestroy() {
        isactivityleft = true;
        super.onDestroy();
    }

    private void GetUrl() {
        AdsAPI apiinterface = APIClient.getClient().create(AdsAPI.class);

        apiinterface.getBtnAd().enqueue(new Callback<Model_Btn>() {
            @Override
            public void onResponse(@NotNull Call<Model_Btn> call, @NotNull Response<Model_Btn> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                checkqureka = response.body().getData().isFlage();
                                if (response.body().getData().isFlage()) {
                                    btnopenad.setVisibility(View.VISIBLE);
                                    id_text.setVisibility(View.VISIBLE);
                                    try {
                                        Glide.with(VideoFolderActivity.this).load(response.body().getData().getImage()).into(btnopenad);
                                        url = response.body().getData().getUrl();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    btnopenad.setOnClickListener(v -> {
                                        try {
                                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                            CustomTabsIntent customTabsIntent = builder.build();
                                            customTabsIntent.intent.setPackage("com.android.chrome");
                                            customTabsIntent.launchUrl(VideoFolderActivity.this, Uri.parse(url));
                                        } catch (Exception e) {
                                            Toast.makeText(VideoFolderActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    btnopenad.setVisibility(View.GONE);
                                    id_text.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Model_Btn> call, @NotNull Throwable t) {
            }
        });
    }


    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(VideoFolderActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }


    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }

}