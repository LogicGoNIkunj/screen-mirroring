package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.APIClient;
import com.screenmirroring.screencast.ads.AdsAPI;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.models.Model_Btn;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThemeActivity extends AppCompatActivity {


    public static int click;
    ImageView image1, image2, image3, image4, image5, image6, image7, image8, image9, image10;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;

    //native
    public NativeAd nativeAds;
    FrameLayout frameLayout;
    FrameLayout one_frameLayout;
    FrameLayout two_frameLayout;

    //interstitial
    public InterstitialAd minterstitialAd;
    boolean isactivityleft;

    ImageView btnopenad;
    TextView id_text;
    public String qurekaimage = "", querekatext = "", url = "";

    public boolean checkqureka = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        isactivityleft = false;
        loadInterstitialAd();
        btnopenad = findViewById(R.id.btnopenad);
        id_text = findViewById(R.id.id_text);
        GetUrl();
        findViewById(R.id.back).setOnClickListener(view -> onBackPressed());

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        image6 = findViewById(R.id.image6);
        image7 = findViewById(R.id.image7);
        image8 = findViewById(R.id.image8);
        image9 = findViewById(R.id.image9);
        image10 = findViewById(R.id.image10);

        Glide.with(this).load(R.drawable.bg1).into(image1);
        Glide.with(this).load(R.drawable.bg2).into(image2);
        Glide.with(this).load(R.drawable.bg3).into(image3);
        Glide.with(this).load(R.drawable.bg4).into(image4);
        Glide.with(this).load(R.drawable.bg5).into(image5);
        Glide.with(this).load(R.drawable.bg6).into(image6);
        Glide.with(this).load(R.drawable.bg7).into(image7);
        Glide.with(this).load(R.drawable.bg8).into(image8);
        Glide.with(this).load(R.drawable.bg9).into(image9);
        Glide.with(this).load(R.drawable.bg10).into(image10);

        loadBanner();
        frameLayout = findViewById(R.id.fl_adplaceholder);
        one_frameLayout = findViewById(R.id.fl_adplaceholder2);
        two_frameLayout = findViewById(R.id.fl_adplaceholder3);
        loadNative();
        loadNative2();
        loadNative3();

        this.image1.setOnClickListener(new image1());
        this.image2.setOnClickListener(new image2());
        this.image3.setOnClickListener(new image3());
        this.image4.setOnClickListener(new image4());
        this.image5.setOnClickListener(new image5());
        this.image6.setOnClickListener(new image6());
        this.image7.setOnClickListener(new image7());
        this.image8.setOnClickListener(new image8());
        this.image9.setOnClickListener(new image9());
        this.image10.setOnClickListener(new image10());

    }

    public class image1 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 11;
            ThemeActivity.this.intent();
        }
    }

    public class image2 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 12;
            ThemeActivity.this.intent();
        }
    }

    public class image3 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 15;
            ThemeActivity.this.intent();
        }
    }

    public class image4 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 16;
            ThemeActivity.this.intent();
        }
    }

    public class image5 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 0;
            ThemeActivity.this.intent();
        }
    }

    public class image6 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 19;
            ThemeActivity.this.intent();
        }
    }

    public class image7 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 3;
            ThemeActivity.this.intent();
        }
    }

    public class image8 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 4;
            ThemeActivity.this.intent();
        }
    }

    public class image9 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 7;
            ThemeActivity.this.intent();
        }
    }

    public class image10 implements View.OnClickListener {
        public void onClick(View view) {
            ThemeActivity.click = 8;
            ThemeActivity.this.intent();
        }
    }

    public void intent() {
        Intent intent = new Intent(ThemeActivity.this, VideoFolderActivity.class);
        intent.putExtra("position", click);
        startActivity(intent);
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(ThemeActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);


        AdRequest adRequest = new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @SuppressLint("MissingPermission")
    private void loadNative() {
        AdLoader.Builder builder = new AdLoader.Builder(ThemeActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.unified_nativead, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka(frameLayout);
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    @SuppressLint("MissingPermission")
    private void loadNative2() {
        AdLoader.Builder builder = new AdLoader.Builder(ThemeActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.unified_nativead, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            one_frameLayout.removeAllViews();
            one_frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka(one_frameLayout);
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    @SuppressLint("MissingPermission")
    private void loadNative3() {
        AdLoader.Builder builder = new AdLoader.Builder(ThemeActivity.this, MyApplication.get_Admob_native_Id());
        builder.forNativeAd(unifiedNativeAd -> {
            if (nativeAds != null) {
                nativeAds.destroy();
            }
            nativeAds = unifiedNativeAd;
            @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(this).inflate(R.layout.unified_nativead, null, false);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            two_frameLayout.removeAllViews();
            two_frameLayout.addView(adView);
        });

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                if (checkqureka) {
                    qureka(two_frameLayout);
                }
            }
        }).build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {

        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                                loadInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                                adError.getMessage();
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
                loadAdError.getMessage();
            }
        });
    }

    private void GetUrl() {
        AdsAPI apiinterface = APIClient.getClient().create(AdsAPI.class);

        apiinterface.getBtnAd().enqueue(new Callback<Model_Btn>() {
            @Override
            public void onResponse(@NotNull Call<Model_Btn> call, @NotNull Response<Model_Btn> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                checkqureka = response.body().getData().isFlage();
                                if (response.body().getData().isFlage()) {
                                    btnopenad.setVisibility(View.VISIBLE);
                                    id_text.setVisibility(View.VISIBLE);
                                    querekatext = response.body().getData().getTitle();
                                    qurekaimage = response.body().getData().getImage();
                                    try {
                                        Glide.with(ThemeActivity.this).load(response.body().getData().getImage()).into(btnopenad);
                                        url = response.body().getData().getUrl();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    btnopenad.setOnClickListener(v -> {
                                        try {
                                            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                            CustomTabsIntent customTabsIntent = builder.build();
                                            customTabsIntent.intent.setPackage("com.android.chrome");
                                            customTabsIntent.launchUrl(ThemeActivity.this, Uri.parse(url));
                                        } catch (Exception e) {
                                            Toast.makeText(ThemeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    btnopenad.setVisibility(View.GONE);
                                    id_text.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Model_Btn> call, @NotNull Throwable t) {
            }
        });
    }

    public void qureka(FrameLayout linearLayout) {
        View view = LayoutInflater.from(this).inflate(R.layout.qureka_native_ads, null);
        TextView ad_call_to_action = view.findViewById(R.id.ad_call_to_action);
        linearLayout.removeAllViews();
        linearLayout.addView(view);
        try {
            if (!qurekaimage.equals("")) {
                Glide.with(this).load(qurekaimage).into((ImageView) view.findViewById(R.id.ad_app_icon));
                ((TextView) view.findViewById(R.id.ad_headline)).setText(querekatext);
                ad_call_to_action.setText(querekatext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ad_call_to_action.setOnClickListener(view1 -> {
            try {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.intent.setPackage("com.android.chrome");
                customTabsIntent.launchUrl(ThemeActivity.this, Uri.parse(url));
            } catch (Exception e) {
                Toast.makeText(ThemeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onDestroy() {
        isactivityleft = true;
        if (this.nativeAds != null) {
            this.nativeAds.destroy();
        }
        super.onDestroy();
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(ThemeActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }

}