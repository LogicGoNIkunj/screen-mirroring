package com.screenmirroring.screencast.activity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.MyApplication;

public class BlinkScreenActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;

    //interstitial
    InterstitialAd minterstitialAd;
    boolean isactivityleft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blink_screen);

        isactivityleft = false;
        loadInterstitialAd();
        relativeLayout = findViewById(R.id.relative);
        loadBanner();

        ObjectAnimator animator = ObjectAnimator.ofInt(relativeLayout, "backgroundColor", Color.WHITE, Color.BLACK);
        animator.setDuration(600);
        animator.setEvaluator(new ArgbEvaluator());
        animator.setRepeatCount(Animation.INFINITE);
        animator.start();

        findViewById(R.id.close).setOnClickListener(view -> {
            animator.cancel();
            onBackPressed();
        });

    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);

        AdRequest adRequest = new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
            }
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(BlinkScreenActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isactivityleft = true;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }
}