package com.screenmirroring.screencast.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.screenmirroring.screencast.R;
import com.screenmirroring.screencast.ads.MyApplication;
import com.screenmirroring.screencast.classes.PlayerUtils;

public class ColorScreenActivity extends AppCompatActivity {

    GestureDetector mGestureDetector;
    protected float mBrightness;
    RelativeLayout relativeLayout;

    private float downX;
    private float downY;
    final int[] colors = {Color.WHITE, Color.BLUE, Color.YELLOW, Color.CYAN, Color.RED, Color.GREEN, Color.BLACK, Color.WHITE};
    int i = 0;

    //banner
    public FrameLayout adContainerView;
    public AdView adView;

    //interstitial
    InterstitialAd minterstitialAd;
    boolean isactivityleft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_screen);

        isactivityleft = false;
        loadInterstitialAd();
        relativeLayout = findViewById(R.id.relative);
        loadBanner();
        findViewById(R.id.close).setOnClickListener(view -> onBackPressed());
        initView();

    }

    private void initView() {
        mBrightness = PlayerUtils.scanForActivity(this).getWindow().getAttributes().screenBrightness;
        mBrightness = ColorScreenActivity.this.getWindow().getAttributes().screenBrightness;
        slideToChangeBrightness(0);
        mGestureDetector = new GestureDetector(this, new MyGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
            }
            break;

            case MotionEvent.ACTION_UP: {
                float upX = event.getX();
                float upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                //HORIZONTAL SCROLL
                if (Math.abs(deltaX) > Math.abs(deltaY)) {
                    int min_distance = 100;
                    if (Math.abs(deltaX) > min_distance) {
                        // left or right
                        if (deltaX < 0) {
                            this.onLeftToRightSwipe();
                        }
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe();
                        }
                    }
                }
            }
            break;
        }

        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public void onLeftToRightSwipe() {

        if (i > 0) {
            i--;
            relativeLayout.setBackgroundColor(colors[i]);

            if (colors[i] == Color.BLACK) {
                ((TextView) findViewById(R.id.brightnessText)).setTextColor(Color.WHITE);
                ((TextView) findViewById(R.id.colorText)).setTextColor(Color.WHITE);
            } else {
                ((TextView) findViewById(R.id.brightnessText)).setTextColor(Color.BLACK);
                ((TextView) findViewById(R.id.colorText)).setTextColor(Color.BLACK);
            }

        }

    }

    public void onRightToLeftSwipe() {

        if (i < colors.length - 1) {
            i++;
            relativeLayout.setBackgroundColor(colors[i]);

            if (colors[i] == Color.BLACK || colors[i] == Color.BLUE) {
                ((TextView) findViewById(R.id.brightnessText)).setTextColor(Color.WHITE);
                ((TextView) findViewById(R.id.colorText)).setTextColor(Color.WHITE);
            } else {
                ((TextView) findViewById(R.id.brightnessText)).setTextColor(Color.BLACK);
                ((TextView) findViewById(R.id.colorText)).setTextColor(Color.BLACK);
            }

        }

    }

    protected class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        private boolean mFirstTouch;
        private boolean mChangeBrightness;

        @Override
        public boolean onDown(MotionEvent e) {
            mBrightness = ColorScreenActivity.this.getWindow().getAttributes().screenBrightness;
            mFirstTouch = true;
            mChangeBrightness = false;
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (e1 == null || e2 == null) return false;
            float deltaY = e1.getY() - e2.getY();
            if (mFirstTouch) {
                if (Math.abs(distanceX) < Math.abs(distanceY)) {
                    mChangeBrightness = true;
                }
                mFirstTouch = false;
            }
            if (mChangeBrightness) {
                slideToChangeBrightness(deltaY);
            }

            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }

    }

    protected void slideToChangeBrightness(float deltaY) {
        Window window = PlayerUtils.scanForActivity(this).getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        int height = PlayerUtils.getScreenHeight(getApplicationContext(), false);
        if (mBrightness == -1.0f) mBrightness = 0.5f;
        float brightness = deltaY * 2 / height * 1.0f + mBrightness;
        if (brightness < 0) {
            brightness = 0f;
        }
        if (brightness > 1.0f) brightness = 1.0f;
        attributes.screenBrightness = brightness;
        window.setAttributes(attributes);
    }

    public void loadBanner() {

        adContainerView = findViewById(R.id.adContainerView);
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    public void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                minterstitialAd = interstitialAd;
                minterstitialAd.setFullScreenContentCallback(
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                MyApplication.appOpenAdManager.isAdShow = false;
                                minterstitialAd = null;
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                MyApplication.appOpenAdManager.isAdShow = true;
                            }
                        });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                MyApplication.appOpenAdManager.isAdShow = false;
                minterstitialAd = null;
            }
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onBackPressed() {
        try {
            if (minterstitialAd != null && !isactivityleft) {
                minterstitialAd.show(ColorScreenActivity.this);
                MyApplication.appOpenAdManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        isactivityleft = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isactivityleft = true;
    }

    public void onPause() {
        super.onPause();
        this.isactivityleft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isactivityleft = true;
    }
}