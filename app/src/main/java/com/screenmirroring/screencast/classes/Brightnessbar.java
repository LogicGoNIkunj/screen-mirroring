package com.screenmirroring.screencast.classes;

import android.app.Activity;
import android.content.ContentResolver;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.SeekBar;

public class Brightnessbar implements SeekBar.OnSeekBarChangeListener{

    public final SeekBar seekbar;
    public final Activity activity;

    public Brightnessbar(Activity activity, SeekBar seekBar) {
        this.activity = activity;
        this.seekbar = seekBar;
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.seekbar.setProgress(i);
        ContentResolver contentResolver = this.activity.getContentResolver();
        Double.isNaN(i);
        int i2 = (int) ((double) i * 2.55d);
        Settings.System.putInt(contentResolver, "screen_brightness", i2);
        WindowManager.LayoutParams attributes = this.activity.getWindow().getAttributes();
        attributes.screenBrightness = ((float) i2) / 255.0f;
        this.activity.getWindow().setAttributes(attributes);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

}
