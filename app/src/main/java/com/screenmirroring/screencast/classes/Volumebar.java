package com.screenmirroring.screencast.classes;

import android.media.AudioManager;
import android.widget.SeekBar;

public class Volumebar implements SeekBar.OnSeekBarChangeListener{

    public final SeekBar seekbar;
    public final AudioManager audioManager;

    public Volumebar(SeekBar seekBar, AudioManager audioManager) {
        this.seekbar = seekBar;
        this.audioManager = audioManager;
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.seekbar.setProgress(i);
        AudioManager audioManager = this.audioManager;
        Double.isNaN((double) i);
        audioManager.setStreamVolume(3, (int) ((double) i / 6.66d), 0);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

}
